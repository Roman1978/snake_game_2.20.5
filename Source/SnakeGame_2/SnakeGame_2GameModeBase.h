// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGame_2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_2_API ASnakeGame_2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
